def principal():
    t = 3
    n = [0, 4, 2]
    calculando_fibonacci(t, n)


def calculando_fibonacci(t, n):
    for i in range(0, t):
        fibonacci = [0, 1]
        x = 0
        if 0 <= n[i] <= 60:
            for j in range(0, n[i]+1):
                if j >= 2:
                    x = fibonacci[j-2] + fibonacci[j-1]
                    fibonacci.append(x)
                x = fibonacci[j]
        else:
            raise ValueError('Tamanho da série de Fibonacci inválido - tente valor entre 0 e 60')
        print('Fib({}): {}'.format(n[i], x))


if __name__ == '__main__':
    principal()

# Em todos os códigos que faço tenho o costume de tentar dividir em várias funções, a função principal que é onde ficam
# executados os exemplos, e as funções que fazem algum cálculo, ou executam alguma função. No caso do programa de
# Fibonacci eu fiz apenas a função principal e uma que calcula a série de Fibonacci. Na função principal coloquei apenas
# as variáveis T e uma lista N com números inteiros dentro que coloquei como minúsculas porque o PyCharm que é o editor
# que utilizei aconselha não utilizar letras maiúsculas para variaveis dentro de funções e chamei a função que criei
# para calcular a série de Fibonacci chamada de calcula_fibonacci e coloquei as variaveis t e n dentro.
# A função calcula_fibonacci tem como entrada T e N minúsculas (t e n), dentro da função criei um laço for com uma
# variável i que tem como início 0 e vai até o valor (t - 1), dentro desse laço for criei a lista chamada fibonacci onde
# ja temos os dois primeiros elementos da série que são 0 e 1 e criei a variavel x que é igual a 0 a cada iteração.
# Dentro desse laço criei a condição onde pedem que a váriavel n só possa ser uma valor entre 0 e 60, caso esteja fora
# desse intervalo, um ValueError é levantado que diz que o tamanho da série de Fibonacci este inválido que deve ser
# entre 0 e 60. Depois criei mais um laço for agora com a váriavel j que tem como início 0 e vai ate n, coloquei mais
# uma condição para que só faça o cálculo para j maior igual a 2, pois os itens (0) e (1) da lista ja existiam. A partir
# dai calculou x sendo o proximo item da série de Fibonacci e adicionava x a lista fibonacci. No final do laço for de j
# colocava x igual fibonacci(j) para pegar o ultimo item da lista. E no final do laço for de i coloquei um print para
# fazer a saída igual pedia o desafio. E utilizo essa condição se __name__ == '__main__' ao final do código, que
# verifica se o arquivo que estou rodando é o '__main__', para poder rodar a função principal toda vez que rodar esse
# código.
